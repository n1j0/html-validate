export { MetaTable } from "./table";
export {
	type ElementTable,
	type MetaAttribute,
	MetaCopyableProperty,
	type MetaData,
	type MetaDataTable,
	type MetaElement,
	type MetaLookupableProperty,
	type PropertyExpression,
	TextContent,
} from "./element";
export { Validator } from "./validator";
